$(document).ready(() => {

  $('.burger').on('click', function () {
    $(this).toggleClass('burger--is-active');
    $('.page-header__bottom').toggleClass('page-header__bottom--is-open');
  });

  $('.tour__dates-item').each(function () {
    $(this).on('click', function () {
      $(this).find('.tour__days').toggleClass('tour__days--is-open');
    });
  });

  var swiper = new Swiper ('.promo__slider', {
    autoplay: { delay: 3000 }
  });

  var swiper = new Swiper ('.tour__promo-slider', {
    autoplay: { delay: 3000 }
  });

  var swiper = new Swiper ('.read-also__slider', {
    pagination: {
      el: '.read-also__slider-progressbar',
      type: 'progressbar'
    },
    navigation: {
      prevEl: '.read-also__slider-navigation .slider-navigation__btn--prev',
      nextEl: '.read-also__slider-navigation .slider-navigation__btn--next'
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 20
      },
      600: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      992: {
        slidesPerView: 3,
        spaceBetween: 40,
      }
    }
  });



  var disclosure = new Houdini('[data-houdini-group="pirates"]', {
    isAccordion: true,
    collapseOthers: true
  });
});